const Campaign = require('../models/campaign');
const logger = require('../config/logger');

module.exports = () => {
    const getAllCampaigns = () =>
        new Promise((resolve, reject) => {
            Campaign.find((error, campaigns) => {
                if (error) {
                    logger.error(error);
                    return reject(error);
                }
                return resolve(campaigns);
            });
        });

    const addCampaign = data =>
        new Promise((resolve, reject) => {
            const campaign = new Campaign(data);
            return campaign.save(error => {
                if (error) {
                    logger.error(error);
                    return reject(error);
                }
                resolve(campaign);
            });
        });

    const updateCampaign = (data, params) =>
        new Promise((resolve, reject) => {
            const id = params.campaign_id;
            Campaign.findByIdAndUpdate(id, data, (error, campaign) => {
                if (error) {
                    logger.error(error);
                    return reject(error);
                }
                return resolve(campaign);
            });
        });

    const getCampaignById = campaignId =>
        new Promise((resolve, reject) => {
            Campaign.findById(campaignId, (error, campaign) => {
                if (error) {
                    logger.error(error);
                    return reject(error);
                }
                return resolve(campaign);
            });
        });
    const getCampaignByUser = userId =>
        new Promise((resolve, reject) => {
            Campaign.find({ user_id: userId }, (error, campaigns) => {
                if (error) {
                    logger.error(error);
                    return reject(error);
                }
                return resolve(campaigns);
            });
        });

    const removeCampaign = campaignId =>
        new Promise((resolve, reject) => {
            Campaign.remove(
                {
                    _id: campaignId
                },
                error => {
                    if (error) {
                        logger.error(error);
                        return reject(error);
                    }
                    return resolve({
                        success: true,
                        _id: campaignId
                    });
                }
            );
        });

    return {
        repo: {
            getAllCampaigns,
            getCampaignById,
            getCampaignByUser,
            addCampaign,
            updateCampaign,
            removeCampaign
        }
    };
};
