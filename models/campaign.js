const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Campaign = new Schema({
    name: { type: String, required: true },
    user_id: { type: String, required: true },
    content: { type: String, required: true },
    startDate: { type: Number, required: true },
    endDate: { type: Number, required: true },
    deadline_type: { type: Number, required: true },
    is_launched: { type: Boolean, default: false },
    summary: { type: String, required: true },
    deadline_run_for: { type: Number, required: false },
    media_id: String
});
module.exports = mongoose.model('Campaign', Campaign);
