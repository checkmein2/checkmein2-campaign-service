const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const api = require('../api/campaigns');
const repository = require('../repository/campaign');
const config = require('../config/config');
const logger = require('../config/logger');
const health = require('../api/health');
const jwt = require('express-jwt');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../docs/swagger.json');
const status = require('http-status');

const app = express();
const mongoose = require('mongoose');
mongoose.connect(config.db);

app.use(
    jwt({
        secret: config.secretKey,
        credentialsRequired: false,
        getToken: function fromHeaderOrQuerystring(req) {
            if (req.headers.authorization !== '') {
                return req.headers.authorization;
            }
            return null;
        }
    })
);
app.use('/campaigns/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((err, req, res, next) => {
    logger.error({
        message: err.message,
        error: err
    });
    res.status(err.status || status.INTERNAL_SERVER_ERROR);
    res.json({
        message: err.message,
        error: process.env.NODE_ENV === 'development' ? err : {}
    });
    next();
});

health(app);
api(app, repository());
app.listen(config.port, () => {
    logger.info('Campaign service has started on port: ', config.port);
});

module.exports = app;
