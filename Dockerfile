FROM node:7.7.3-alpine

# Create app directory
RUN mkdir -p /app
WORKDIR /app

# Install app dependencies
COPY package.json /app/

# Bundle app source
RUN yarn install

COPY . /app
ENV PORT 3001
EXPOSE 3001
CMD [ "yarn", "start" ]