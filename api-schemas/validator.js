const Ajv = require('ajv');
const ajv = Ajv({ allErrors: true, removeAdditional: 'all' });
const createCampaignSchema = require('./create-campaign');

ajv.addSchema(createCampaignSchema, 'create-campaign');

const errorResponse = schemaErrors => {
    const errors = schemaErrors.map(error => {
        return {
            path: error.dataPath,
            message: error.message
        };
    });
    return {
        status: 'failed',
        errors
    };
};

/**
 * Validates incoming request bodies against the given schema,
 * providing an error response when validation fails
 * @param  {String} schemaName - name of the schema to validate
 * @return {Object} response
 */
const validateSchema = schemaName => (req, res, next) => {
    const valid = ajv.validate(schemaName, req.body);
    if (!valid) {
        return res.send(errorResponse(ajv.errors));
    }
    next();
};

module.exports = validateSchema;
