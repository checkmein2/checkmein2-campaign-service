/* eslint-disable import/no-absolute-path*/
const secrets = require('/run/secrets/app_secrets');
/* eslint-enable import/no-absolute-path*/

const config = {
    port: process.env.PORT || 3001,
    db: `mongodb://${process.env.MONGO_URL}/Campaign`,
    LOG_URL: process.env.LOG_URL || 'logs5.papertrailapp.com',
    LOG_PORT: process.env.LOG_PORT || 39491,
    secretKey: secrets.SECRET_KEY || 'secret_key'
};

module.exports = config;
