const status = require('http-status');
const validateSchema = require('../api-schemas/validator');
const logger = require('../config/logger');

module.exports = (app, options) => {
    const { repo } = options;

    const authenticate = (req, res, next) => {
        if (!req.user) {
            logger.error(`401 METHOD: ${req.method} URL: ${req.url}`);

            res
                .status(status.UNAUTHORIZED)
                .json({ error: 'UNAUTHORIZED REQUEST' });
        } else {
            next();
        }
    };

    app.get('/campaigns', (req, res) => {
        repo.getAllCampaigns().then(campaigns => {
            res.status(status.OK).json(campaigns);
        });
    });

    app.get('/campaigns/:campaign_id', (req, res, next) => {
        repo
            .getCampaignById(req.params.campaign_id)
            .then(campaign => {
                res.status(status.OK).json(campaign);
            })
            .catch(next);
    });
    app.get('/campaigns/user/:user_id', (req, res, next) => {
        repo
            .getCampaignByUser(req.params.user_id)
            .then(campaigns => {
                res.status(status.OK).json(campaigns);
            })
            .catch(next);
    });

    app.post('/campaigns', authenticate, validateSchema('create-campaign'), (
        req,
        res,
        next
    ) => {
        const campaign = Object.assign(req.body.campaign, {
            user_id: req.user.id
        });
        repo
            .addCampaign(campaign)
            .then(campaigns => {
                res.status(status.OK).json(campaigns);
            })
            .catch(next);
    });
    app.put('/campaigns/:campaign_id', authenticate, (req, res, next) => {
        const newCampaign = Object.assign(req.body.campaign, {
            user_id: req.user.id
        });
        repo
            .updateCampaign(newCampaign, req.body.params)
            .then(campaign => {
                res.status(status.OK).json(campaign);
            })
            .catch(next);
    });
    app.delete('/campaigns/:campaign_id', (req, res, next) => {
        repo
            .removeCampaign(req.params.campaign_id)
            .then(campaign => {
                res.status(status.OK).json(campaign);
            })
            .catch(next);
    });
};
